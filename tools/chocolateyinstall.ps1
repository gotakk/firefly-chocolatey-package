﻿$ErrorActionPreference = 'Stop';
$url64         = 'https://dl.firefly.iota.org/firefly-desktop-2.1.9.exe'
$checksum64    = '0ce5d21d5d6b056fb3012e77e53dc1f6d30949395fb0c7e848ca288dbf6b33f1'

$FireflyPackageArgs = @{
  packageName   = $env:ChocolateyPackageName
  fileType      = 'exe'
  silentArgs    = '/S --force-run'
  url64bit      = $url64
  softwareName  = 'firefly*'
  checksum64    = $checksum64
  checksumType64= 'sha256'
  validExitCodes= @(0, 3010, 1641)
}

Install-ChocolateyPackage @FireflyPackageArgs
